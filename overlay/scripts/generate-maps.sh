#!/bin/sh
IFS=' '
test=$(which jq);
out=$?
if [ $out -gt 0 ] ; then
	echo "This script requires jq to be installed."
	echo "Your package manager should be able to find it"
	exit 1
fi

path=$(mktemp -d)
outputfile=${path}/outfile.conf

echo "map \$http_host \$cacheidentifier {" >> $outputfile
echo "    hostnames;" >> $outputfile
echo "    default \$http_host;" >> $outputfile

wget https://raw.githubusercontent.com/uklans/cache-domains/master/cache_domains.json -qO ${path}/cache_domains.json
jq -r '.cache_domains | to_entries[] | .key' ${path}/cache_domains.json | while read entry; do 
	key=$(jq -r ".cache_domains[$entry].name" ${path}/cache_domains.json)
	jq -r ".cache_domains[$entry].domain_files | to_entries[] | .key" ${path}/cache_domains.json | while read fileid; do
		jq -r ".cache_domains[$entry].domain_files[$fileid]" ${path}/cache_domains.json | while read filename; do
			echo "" >> $outputfile
			wget https://raw.githubusercontent.com/uklans/cache-domains/master/${filename} -qO ${path}/${filename}
			cat ${path}/${filename} | while read fileentry; do
				# Ignore comments
				case "$var" in
				    \#*) continue ;;
				esac
				# parsed=$(echo $fileentry | sed -e "s/^\*\.//")
				if grep -q "$fileentry" $outputfile; then
					continue
				fi
				echo "    ${fileentry} ${key};" >> $outputfile
			done
		done
	done
done
echo "}" >> $outputfile
cat $outputfile
rm -rf $path