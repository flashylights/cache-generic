#!/bin/sh
set -e
mkdir -m 755 -p /data/cache
mkdir -m 755 -p /data/info
mkdir -m 755 -p /data/logs
mkdir -m 755 -p /tmp/nginx/
chown -R ${WEBUSER}:${WEBUSER} /data/

sed -i "s/CACHE_MEM_SIZE/${CACHE_MEM_SIZE}/"  /etc/nginx/sites-available/generic.conf
sed -i "s/CACHE_DISK_SIZE/${CACHE_DISK_SIZE}/" /etc/nginx/sites-available/generic.conf
sed -i "s/CACHE_MAX_AGE/${CACHE_MAX_AGE}/"    /etc/nginx/sites-available/generic.conf

echo "Checking permissions (This may take a long time if the permissions are incorrect on large caches)..."
find /data \! -user ${WEBUSER} -exec chown ${WEBUSER}:${WEBUSER} '{}' +

echo "Generating up-to-date cache mappings"
/bin/sh /scripts/generate-maps.sh

echo "Done. Starting caching server."

/usr/sbin/nginx -t

/usr/sbin/nginx -g "daemon off;"
